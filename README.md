<!--- 
SPDX-FileCopyrightText: 2021 Daniel Aleksandersen <https://www.daniel.priv.no/>
SPDX-License-Identifier: CC0-1.0
-->

`url-open` is a simple a program for FreeDesktop environments (Linux, FreeBSD) for opening `.URL` files
([Internet Shortcut files](https://docs.microsoft.com/en-us/windows/win32/lwef/internet-shortcuts)).
The program parses opened `.URL` files, opens their link in your default web browser, and immidiately exits.
It installs as a helper program. All you need to do is to double-click your `.URL` files to open.

Links will open in your default web browser using `xdg-open`.


# Installation instructions

1. Downloal and extract [a release tarball](https://codeberg.org/da/url-open/releases)
2. `sudo make install --prefix=/usr/local`
