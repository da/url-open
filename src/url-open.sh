#!/bin/bash
# SPDX-FileCopyrightText: 2021 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: CC0-1.0

if test ! -f "$1" -o ! -r "$1"
then
  >&2 echo "File '$1' doesn't exist or isn't readable."
  exit 1
fi

URL=`sed -n 's/^URL=//p' "$1" | tr -d ' '`

echo "$URL" | grep -E '[a-z]+:'

if test -z "$URL" -o ! $? -eq 0
then
  >&2 echo "Didn't find an URL in file '$1'."
  exit 1
fi

xdg-open "$URL"
